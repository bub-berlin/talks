# Talks

This repository contains talks and presentations from Bits & Bäume Berlin. 

## License
[CC-BY Bits & Bäume](https://creativecommons.org/licenses/by/2.0/de/)